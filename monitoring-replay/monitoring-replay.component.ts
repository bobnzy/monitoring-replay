import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

export interface YMDhmsDateTime {
  Y: number;
  M: number;
  D: number;
  h: number;
  m: number;
  s: number;
  t: number;
}
/**
 * 监控回放组件
 */
@Component({
  selector: 'app-comm-monitoring-replay',
  templateUrl: './monitoring-replay.component.html',
  styleUrls: ['./monitoring-replay.component.less'],
})
export class MonitoringReplayComponent implements OnInit {
  deviceList = ['IPV1', 'IPV2', 'IPV3'];
  replayList = [1, 2, 3, 4]; // 回放列表
  replayList0 = [1, 2, 3, 4]; // 回放列表0
  replayList1 = [1, 2, 3]; // 回放列表1
  replayList2 = [1, 2, 3, 4, 5]; // 回放列表2
  isActive = 0; // 当前活动项

  isContrallAll = false; // 是否全局控制

  isPlaySynchronously = false; // 是否同步播放
  // 控制栏按钮组
  controlBtnList = [
    { iconClass: 'fast_back-svg', clickFunc: this.fastBack },
    { iconClass: 'back_forward_fill-svg', clickFunc: this.backward },
    { iconClass: 'rewind-svg', clickFunc: this.rewind },
    { iconClass: 'pause-svg', clickFunc: this.pause },
    { iconClass: 'playfill-svg', clickFunc: this.play },
    { iconClass: 'play_forward_fill-svg', clickFunc: this.forward },
    { iconClass: 'fast_forward-svg', clickFunc: this.fastForward },
  ];
  speedList = ['×1/16', '×1/8', '×1/4', '×1/2', '×1', '×2', '×4', '×8', '×16']; // 视频倍速列表
  nowSpeed = 4; // 当前视频倍速
  voice = 50; // 声音
  lastVoice = 0; // 缓存音量
  isMuted = false; // 是否静音

  // 扩展功能按钮组
  extraBtnList = [
    { iconClass: 'cameraadd-svg', clickFunc: this.saveImg },
    { iconClass: 'tag-svg', clickFunc: this.tagIt },
    { iconClass: 'retain-svg', clickFunc: this.retain },
    { iconClass: 'full_screen-svg', clickFunc: this.fullScreen },
  ];

  displayNone = true; // 是否不显示“返回到当前刻度”
  canvas: any; // canvas元素
  mousedownOSX = 0; // canvas鼠标按下时的offsetX
  mousedownOSY = 0; // canvas鼠标按下时的offsetY
  /**
   *  模式：[           总秒数           每五格
   * 1:12*24*3600,    0: 1036800          24h
   * 2:12*12*3600,    1: 518400           12h
   * 3:12*8*3600,     2: 345600           8h
   * 4:12*4*3600,     3: 172800           4h
   * 5:12*2*3600,     4: 86400            2h
   * 6:12*3600,       5: 43200            1h
   * 7:12*30*60,      6: 21600            30m
   * 8:12*10*60,      7: 7200             10m
   * 9:12*5*60,       8: 3600             5m
   * 10:12*3*60,      9: 2160             3m
   * 11:12*60,        10: 720             1m
   * 12:12*30,        11: 360             30s
   * 13:12*10,        12: 120             10s
   * 14:12*5          13: 60              5s
   * ]
   */
  proportionList = [1036800, 518400, 345600, 172800, 86400, 43200, 21600, 7200, 3600, 2160, 720, 360, 120, 60];
  proportion = 8; // 时间刻度线比例0-13
  unitWidth = 25; // canvas刻度条的每小格所占宽度
  nowTickTime!: Date; // 已选择的时间刻度毫秒值
  rewindBeginTime = new Date('2020-12-12 12:12:12'); // 回放开始时间
  rewindEndTime = new Date('2020-12-12 12:22:22'); // 回放结束时间
  originTime!: number; // 实时canvas原点时间豪秒数
  lastOriginTime!: number; // 拖动前的原点时间毫秒数
  clickSpaceX = 0; // 点击落点与前一刻度线差值
  abscissaOffset = 0; // 横坐标轴偏移量
  isCanvaseMouseDown = false; // 是否在canvas上按下了鼠标

  @ViewChild('timeScaleCanvas', { static: true })
  private timeScaleCanvas!: ElementRef<HTMLCanvasElement>;
  private ctx!: CanvasRenderingContext2D | null;
  private canvasWidth!: number;
  private canvasHeight!: number;

  constructor() {}

  ngOnInit(): void {
    this.canvasInit();
  }

  canvasInit(): void {
    // canvas 初始化
    this.canvas = this.timeScaleCanvas.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    this.canvas.width = 1500;
    this.canvas.height = 50;
    this.canvasWidth = this.canvas.width;
    this.canvasHeight = this.canvas.height;
    // 通过有效开始时间初始化画布原点时间秒数
    this.setScaleTime(this.rewindBeginTime);
    // 画canvas时间刻度条
    this.drawCanvas();
  }

  // 鼠标按下，执行拖动事件
  onMouseDown(event: any): any {
    this.mousedownOSX = event.offsetX;
    this.mousedownOSY = event.offsetY;
    this.isCanvaseMouseDown = true;
    this.lastOriginTime = this.originTime;
    // 点击落点横坐标到上一小格刻度线差值
    this.clickSpaceX = this.getClickX(event) % this.unitWidth;
    this.canvas.addEventListener('mousemove', this.dragCanvas, false);
  }
  // 鼠标弹起，与按下位置比较，若有变化则不执行click事件，否则执行canvasClick()
  onMouseUp(event: any): void {
    this.canvas.removeEventListener('mousemove', this.dragCanvas, false);
    if (event.offsetX === this.mousedownOSX && event.offsetY === this.mousedownOSY) {
      this.canvasClick(event);
    }
    this.isCanvaseMouseDown = false;
  }

  // 鼠标离开该区域
  onMouseLeave(e: any): void {
    if (this.isCanvaseMouseDown) {
      this.onMouseUp(e);
    }
  }

  // canvas点击事件
  canvasClick(e: any): void {
    this.nowTickTime = this.axisToTime(this.getClickX(e));
    this.drawNowTick();
  }

  // 获取点击落点横坐标
  getClickX(e: any): number {
    return (this.canvasWidth * e.offsetX) / e.target.offsetWidth;
  }

  // canvas拖动事件
  dragCanvas = (e: any) => {
    const target = e.target || e.srcElement;
    // 拖动横坐标偏移值
    this.abscissaOffset = (this.canvasWidth * (e.offsetX - this.mousedownOSX)) / target.offsetWidth;
    // 拖动豪秒数
    const dragTime = (this.proportionList[this.proportion] * (e.offsetX - this.mousedownOSX) * 1000) / target.offsetWidth;
    this.originTime = this.lastOriginTime - dragTime;
    this.drawCanvasByTime();
    if (this.nowTickTime) {
      this.drawNowTick();
    }
  };

  // 时间转换为Y-M-D-h-m-s
  timeToYMDhms(t: Date): YMDhmsDateTime {
    return {
      Y: t.getFullYear(),
      M: t.getMonth() + 1,
      D: t.getDate(),
      h: t.getHours(),
      m: t.getMinutes(),
      s: t.getSeconds(),
      t: t.getTime(),
    };
  }

  /**
   * 通过有效开始时间 初始化画布原点时间秒数
   * @param t Date 有效开始时间
   */
  setScaleTime(t: Date): void {
    const tt = this.timeToYMDhms(t);
    const ts = tt.t / 1000;
    // 原点秒数=有效开始时间-不满5格最小单位多余秒数-3大格(3*5小格)秒数
    // const os = (ts - ts % (this.proportionList[this.proportion] / 12) - this.proportionList[this.proportion] / 12 * 3);
    const originTime = ts - (ts % (this.proportionList[this.proportion] / 12)) - this.proportionList[this.proportion] / 4;
    this.originTime = originTime * 1000;
  }

  /**
   *  时间转换为横坐标
   *  @param time string 传入时间
   *  @returns axisX 返回横坐标x值
   */
  timeToAxis(time: Date): number {
    const tt = this.timeToYMDhms(time);
    // 总长:总秒 this.canvasWidth/this.proportionList[this.proportion]
    const axisX = this.canvasWidth * ((tt.t - this.originTime) / (this.proportionList[this.proportion] * 1000));
    return axisX;
  }

  /**
   *  横坐标转换为时间
   *  @param x number 横坐标x值
   *  @returns t Date 返回该刻度时间
   */
  axisToTime(x: number): Date {
    const os = this.originTime;
    const ts = os + (this.proportionList[this.proportion] * 1000 * x) / this.canvasWidth;
    const t = new Date(ts);
    return t;
  }

  // 拖动刻度条-根据原点时间画canvas时间刻度条
  drawCanvasByTime(): void {
    if (this.ctx) {
      // 一大格=单元格*5
      const bigUnitWidth = this.unitWidth * 5;
      const ctx = this.ctx;
      ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
      // 原点横坐标偏移值
      const oox = this.abscissaOffset;

      let y = 0;
      for (let i = -this.canvasWidth + oox; i < this.canvasWidth * 2; i += this.unitWidth) {
        if (Math.round(i - oox) % bigUnitWidth === 0) {
          const tt = this.timeToYMDhms(this.axisToTime(i));
          y = this.canvasHeight * 0.4;
          ctx.font = 'bold 16px serif';
          ctx.fillText(`${tt.h}:${tt.m}:${tt.s}`, i, y + 6);
        } else {
          y = this.canvasHeight * 0.6;
        }
        ctx.beginPath();
        ctx.moveTo(i, this.canvasHeight);
        ctx.lineTo(i, y);
        ctx.stroke();
      }
      // 画有效区
      this.drawEffectiveArea();
    }
  }

  // 画canvas时间刻度条
  drawCanvas(): void {
    // 以canvas宽度划线
    if (this.ctx) {
      // 一大格=单元格*5
      const bigUnitWidth = this.unitWidth * 5;
      const ctx = this.ctx;
      let y = 0;
      for (let i = -this.canvasWidth; i < this.canvasWidth * 2; i += this.unitWidth) {
        if (i % bigUnitWidth === 0) {
          const tt = this.timeToYMDhms(this.axisToTime(i));
          y = this.canvasHeight * 0.4;
          ctx.font = 'bold 16px serif';
          ctx.fillText(`${tt.h}:${tt.m}:${tt.s}`, i, y + 6);
        } else {
          y = this.canvasHeight * 0.6;
        }
        ctx.beginPath();
        ctx.moveTo(i, this.canvasHeight);
        ctx.lineTo(i, y);
        ctx.stroke();
      }
      // 画有效区
      this.drawEffectiveArea();
    }
  }

  // 画有效区-绿色背景
  drawEffectiveArea(): void {
    // 有效时间区域转换为横坐标范围
    const begin = this.timeToAxis(this.rewindBeginTime);
    const end = this.timeToAxis(this.rewindEndTime);
    const w = end - begin;
    const ctx = this.ctx;
    if (ctx) {
      ctx.save();
      ctx.fillStyle = 'RGBA(0, 255, 0, .3)';
      ctx.fillRect(begin, 0, w, this.canvasHeight);
      ctx.restore();
    }
  }

  // canvas画当前刻度
  drawNowTick(): void {
    const t = this.timeToYMDhms(this.nowTickTime);
    const x = this.timeToAxis(this.nowTickTime);
    const ctx = this.ctx;
    if (ctx) {
      // 清除与重绘刻度条
      ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
      this.drawCanvasByTime();
      // 当前刻度
      ctx.save();
      ctx.fillStyle = 'red';
      ctx.strokeStyle = 'red';
      ctx.textAlign = 'center';
      ctx.font = 'bold 16px serif';
      ctx.beginPath();
      ctx.moveTo(x, 0);
      ctx.lineTo(x, this.canvasHeight);
      ctx.stroke();
      ctx.stroke();
      ctx.fillRect(x - 5, 0, 10, this.canvasHeight * 0.1);
      ctx.fillText(`${t.Y}-${t.M}-${t.D}  ${t.h}:${t.m}:${t.s}`, x, 14);
      ctx.restore();
    }
  }

  // 切换当前设备列表
  changeDevices(i: number): void {
    this.isActive = i;
    switch (i) {
      case 0:
        this.replayList = this.replayList0;
        break;
      case 1:
        this.replayList = this.replayList1;
        break;
      case 2:
        this.replayList = this.replayList2;
        break;
    }
  }

  // 导出
  exportIt(e: any): void {}
  // 锁定
  lockIt(e: any): void {}
  // 解锁
  unLockIt(e: any): void {}
  // 重新选择
  reChoose(e: any): void {}

  // 截图
  screenshot(e: any): void {}
  // 关闭
  close(e: any): void {}

  // 是否全局控制
  isContrallAllChange(): void {
    this.isContrallAll = !this.isContrallAll;
  }

  // 跳转
  jumpTo(e: any): void {}
  // 同步
  synchronize(): void {
    this.isPlaySynchronously = !this.isPlaySynchronously;
  }
  // 静音
  mute(): void {
    if (this.isMuted) {
      this.voice = this.lastVoice;
    } else {
      this.lastVoice = this.voice;
      this.voice = 0;
    }
    this.isMuted = !this.isMuted;
  }
  // 音量调整
  voiceChange(e: any): void {
    this.voice = e.target.value;
  }

  // 快退
  fastBack(): void {}
  // 上一帧
  backward(): void {}
  // 倒放
  rewind(): void {}
  // 暂停
  pause(): void {}
  // 播放
  play(): void {}
  //  下一帧
  forward(): void {}
  // 快进
  fastForward(): void {}

  // 改变倍速
  speedChange(i: number): void {
    this.nowSpeed = i;
  }

  // 图片存储
  saveImg(): void {}
  // 打标签
  tagIt(): void {}
  // 备份
  retain(): void {}
  // 全屏
  fullScreen(): void {}
}
